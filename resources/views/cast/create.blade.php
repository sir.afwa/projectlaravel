@extends('layout.master')

@section('header')
Halaman Tambah Pemain
    
@endsection
  
@section('title')
    Halaman Tambah Pemain
    @endsection

@section('isi')


<div>
      <form action="/cast" method="POST">
        @csrf
            <div class="form-group">
                <label for="title">Nama Pemain</label>
                <input type="text" class="form-control" name="nama" id="title"  placeholder="Masukkan nama Pemain">
                @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            </div>
            <div class="form-group">
                <label for="title">Umur Pemain</label>
                <input type="number" class="form-control" name="umur" id="title" placeholder="Masukkan umur Pemain">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
            <div class="form-group">
                <label for="title">Biodata Pemain</label>
                <input type="text" class="form-control" name="bio" id="title" placeholder="Masukkan biodata Pemain">
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>

               

                
            <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
        </form>
</div>
@endsection